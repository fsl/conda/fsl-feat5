if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/removeFSLWrapper autoaq checkFEAT cutoffcalc easythresh estnoise feat feat_gm_prepare feat_model featquery featregapply fsl_motion_outliers fslerrorreport generateConfounds invfeatreg mainfeatreg match_smoothing mccutup mp_diffpow.sh perfusion_subtract pnm_evs pnm_stage1 popp prewhiten renderhighres setFEAT tsplot updatefeatreg Feat_gui Featquery_gui Glm_gui Pnm_gui Renderhighres_gui
fi
